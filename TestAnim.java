import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.scene.transform.*;


public class TestAnim extends Application {

      @Override
      public void start(Stage primaryStage) {

	  Circle circ = new Circle();
	  circ.setCenterX(3.0);
	  circ.setCenterY(0.0);
	  circ.setRadius(0.5);
	  circ.setFill(Color.color(0,1,0,0.5));
	  Group anim = new Group();
	  anim.getChildren().addAll(circ);
	  
	  // center にレイアウト
	  BorderPane root = new BorderPane();
	  root.setCenter(anim);
	  anim.getTransforms().add(new Scale(80, -80, 0, 0));

	  //circ.getTransforms().add(new Translate(3,2));
	  // root を Scene に、それを Stage に設定
	  primaryStage.setScene(new Scene(root, 600, 600));
	  primaryStage.show();

	  AnimationTimer timer = new AnimationTimer() {
		  long offset = 0;
		  @Override 
		  public void handle(long now) {
		      if( offset == 0 ) {
			  offset = now;  
		      } else {           
			  double t = (double)( now - offset )*0.5 / 1_000_000_000;
			  circ.setCenterX( 2*Math.cos( 3*t * Math.PI ));
			  circ.setCenterY( 3*Math.sin( 2*t * Math.PI ));
		      }
		  }
	      };

	  timer.start();

      }

    public static void main(String[] args) {
	launch(args);
    }
}
