import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.scene.transform.*;


public class TestDraw extends Application {


      @Override
      public void start(Stage primaryStage) {


	  Circle circ = new Circle();
	  circ.setCenterX(0);
	  circ.setCenterY(0);
	  circ.setRadius(1);
	  circ.setFill(Color.RED);
	  
	  Circle circ2 = new Circle();
	  circ2.setCenterX(-3);
	  circ2.setCenterY(0);
	  circ2.setRadius(1);
	  
	  // 円をGroupに登録
	  Group anim = new Group();
	  anim.getChildren().addAll(circ);
	  anim.getChildren().addAll(circ2);

	  // center にレイアウト
	  BorderPane root = new BorderPane();
	  root.setCenter(anim);
	  anim.getTransforms().add(new Scale(20, 20, 0, 0));

	  circ2.getTransforms().add(new Translate(5,7));
	  // root を Scene に、それを Stage に設定
	  primaryStage.setScene(new Scene(root, 600, 600));
	  primaryStage.show();

      }

    public static void main(String[] args) {
	launch(args);
    }
}
